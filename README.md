# Creating Adapter for reading Excel file as List of T

Main purpose is to expose the data from an excel sheet as a list of objects of a certain class.

Having an excel sheet with the name `testscenario.xlsx` with the sheets `Existing banks` and `Listed providers`
Or having an excel sheet saved as XML Spreadsheet 2003.

Now create the models for `bank`and one for `provider`.

	using Testware.Utilities.TestDataAdapters;
	
	[ExcelSheetName("Existing banks")]
	public class Bank
	{
		[ExcelColumn("Name", true)]
		public string Name { get; set; }

		[ExcelColumn("Address")]
		public string Address { get; set; }
	
		[ExcelColumn("EmployeeCount")]
		public int EmployeeCount { get; set; }
	}

	[ExcelSheetName("Listed providers")]
	public class Providers
	{
		[ExcelColumn("Identifier", true)]
		public long Id { get; set; }

		[ExcelColumn("Postal Code")]
		public string PostalCode { get; set; }
	
		[ExcelColumn("Transport")]
		public string Transport { get; set; }
	}




Now create the adaptor for the testscenario workbook.

	using Testware.Utilities.TestDataAdapters;

	public TestScenarioAdapter : BaseExcelAdapter
	{
		public TestScenarioAdapter(byte[] resource) : base(resource) { }

		public List<Bank> GetBanks()
		{
			var reader = ExcelDataReader<Bank>.Load(resource);
			return reader.AsList();
		}

		public List<Provider> GetProviders()
		{
			var reader = ExcelDataReader<Provider>.Load(resource);
			return reader.AsList();
		}
	}

When having an XML Spreadsheet 2003

	public TestScenarioXmlAdapter : BaseExcelAdapter
	{
		public TestScenarioXmlAdapter(string resource) : base(resource) { }

		public List<Bank> GetBanks()
		{
			var reader = ExcelDataReader<Bank>.Load(resource);
			return reader.AsList();
		}

		public List<Provider> GetProviders()
		{
			var reader = ExcelDataReader<Provider>.Load(resource);
			return reader.AsList();
		}
	}


Now consume the Adaptor for reading the data from the excel sheet


	[Test]
	public void UseMyNewAdapter()
	{
		var adapter = new TestScenarioAdapter(Properties.Resources.TestScenarioXlsx);
		Assert.That(adapter.GetBanks().Count, Is.EqualTo(20));
	}

	[Test]
	public void UseMyNewAdapter()
	{
		var adapter = new TestScenarioXmlAdapter(Properties.Resources.TestScenarioXml);
		Assert.That(adapter.GetBanks().Count, Is.EqualTo(20));
	}
