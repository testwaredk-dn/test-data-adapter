@echo off
IF "%1"=="" goto usage

for /D %%i in (%~dp0src\packages\NUnit.Runners.*) do set tools=%%i\tools\

"%tools%nunit-console.exe" "src\Testware.Utilities.TestDataAdapterTests\bin\Debug\Testware.Utilities.TestDataAdaptersTests.dll" /result "%1-1.xml"

 goto exit
 
 :usage
 echo Usage: run-unittest [result xml file path without any extension]
 
 :exit