﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Testware.Utilities.TestDataAdapters;

namespace Testware.Utilities.TestDataAdaptersTests.Models
{
    [ExcelSheetName("Maintain meta data item")]    
    public class MetaDataItem
    {

        [ExcelColumn("Predefined enity")]
        public string PredefinedEntity { get; set; }

        [ExcelColumn("Entity")]
        public string Entity { get; set; }
        
        [ExcelColumn("Reg. no.")]
        public int RegistrationNumber { get; set; }

        [ExcelColumn("CDIdent")]
        public long CDIdent { get; set; }
        
    }
}
