﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Testware.Utilities.TestDataAdapters;

namespace Testware.Utilities.TestDataAdaptersTests.Models
{

    [ExcelSheetName("Maintain institution")]
    public class Institution
    {
        [ExcelColumn("Testname", true)]
        public string Testname { get; set; }

        [ExcelColumn("Regi. name")]
        public string RegistrationName { get; set; }

        [ExcelColumn("Reg. number")]
        public long RegistrationNumber { get; set; }

        [ExcelColumn("Short name")]
        public string ShortName { get; set; }

    }
}
