﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Testware.Utilities.TestDataAdapters;

namespace Testware.Utilities.TestDataAdaptersTests.Models
{
    [ExcelSheetName("Sheet1")]
    public class AllTypes
    {
        [ExcelColumn("String")]
        public string StringColumn { get; set; }

        [ExcelColumn("Long")]
        public long LongColumn { get; set; }

        [ExcelColumn("Int")]
        public int IntColumn { get; set; }

        [ExcelColumn("Date")]
        public DateTime DateColumn { get; set; }

        [ExcelColumn("Bool")]
        public bool BoolColumn { get; set; }

        [ExcelColumn("Double")]
        public double DoubleColumn { get; set; }

    }
}
