﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Testware.Utilities.TestDataAdapters;

namespace Testware.Utilities.TestDataAdaptersTests.Models
{

    [ExcelSheetName("Maintain settlement participant")]
    public class SettlementParticipant
    {
        [ExcelColumn("Settlement agreement")]
        public string SettlementAgreement { get; set; }

        [ExcelColumn("Select participant")]
        public string SelectParticipant { get; set; }

        [ExcelColumn("Radiobutton")]
        public string RadioButton { get; set; }

        [ExcelColumn("Successful single FSI")]
        public bool SuccessfulSingleFSI { get; set; }
    }
}
