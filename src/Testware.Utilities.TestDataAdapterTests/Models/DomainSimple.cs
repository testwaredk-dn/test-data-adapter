﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Testware.Utilities.TestDataAdapters;

namespace Testware.Utilities.TestDataAdaptersTests.Models
{
    [ExcelSheetName("Sheet1")]
    public class DomainSimple
    {
        [ExcelColumn("Description")]
        public string Description { get; set; }

        [ExcelColumn("Identifier", true)]
        public string Identifier { get; set; }

        [ExcelColumn("Central Security Officer")]
        public bool? CentralSecurityOfficer { get; set; }

        [ExcelColumn("Central Administrator")]
        public bool? CentralAdministrator { get; set; }

        [ExcelColumn("RTGS")]
        public string RTGS { get; set; }

        [ExcelColumn("ASA")]
        public string ASA { get; set; }

        [ExcelColumn("Access")]
        public string Access { get; set; }

        [ExcelColumn("CB")]
        public string CB { get; set; }

        [ExcelColumn("SB")]
        public string SB { get; set; }

        public string extra { get; set; }

    }
}
