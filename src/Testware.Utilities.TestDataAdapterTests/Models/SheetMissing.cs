﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Testware.Utilities.TestDataAdapters;

namespace Testware.Utilities.TestDataAdaptersTests.Models
{

    [ExcelSheetName("Missing Sheet Name")]
    public class SheetMissing
    {
        [ExcelColumn("Entity")]
        public string Entity { get; set; }

    }
}
