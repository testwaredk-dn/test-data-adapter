﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Testware.Utilities.TestDataAdapters;

namespace Testware.Utilities.TestDataAdaptersTests.Models
{
    [ExcelSheetName("Maintain institution")]
    public class MaintainInstitution
    {
        [ExcelColumn("Registered name", true)]
        public string RegisteredName { get; set; }
        
        [ExcelColumn("Registration number")]
        public string RegistrationNumber { get; set; }
        
        [ExcelColumn("Short name", true)]
        public string ShortName { get; set; }

        [ExcelColumn("Designation", true)]
        public string Designation { get; set; }

    }
}
