﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Testware.Utilities.TestDataAdapters;

namespace Testware.Utilities.TestDataAdaptersTests.Models
{
    [ExcelSheetName("Maintain Settlement Bank")]
    public class SettlementBank
    {
        [ExcelColumn("Institution", true)]
        public string Institution { get; set; }

        [ExcelColumn("Registered name")]
        public string RegisteredName { get; set; }

        [ExcelColumn("BIC code")]
        public string BICCode { get; set; }

        [ExcelColumn("Ref. short code")]
        public string RefShortCode { get; set; }

        [ExcelColumn("Short name")]
        public string ShortName { get; set; }

        [ExcelColumn("Charge category")]
        public string ChargeCategory { get; set; }

        [ExcelColumn("Interest category")]
        public string InterestCategory { get; set; }

        [ExcelColumn("Utilise A/C")]
        public bool UtiliseAC { get; set; }

        [ExcelColumn("Securing AC account number")]
        public string SecuringACAccountNumber { get; set; }

        [ExcelColumn("Technical infra. agent")]
        public string TechnicalInfraAgent { get; set; }

        [ExcelColumn("Y-Copy FSI rec.")]
        public string YCopyFSIRec { get; set; }

    }
}
