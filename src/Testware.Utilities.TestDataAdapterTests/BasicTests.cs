﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using NUnit.Framework;
using Excel;
using Testware.Utilities.TestDataAdapters;
using Testware.Utilities.TestDataAdaptersTests.Adapters;
using Testware.Utilities.TestDataAdaptersTests.Models;
using System.Reflection;

namespace Testware.Utilities.TestDataAdaptersTests
{
    public class BasicTests
    {

        [Test]
        public void connection_to_excel()
        {
            ExcelDataReader<DomainSimple> list = ExcelDataReader<DomainSimple>.Load(Properties.Resources.domain_simple);
            List<DomainSimple> domainList = list.AsList();
        }

    
        [Test]
        public void using_exceldatalist_generic()
        {
            var adapter = TestDataFacade.GetDomainSimpleAdapter();
            List<DomainSimple> domainList = adapter.GetDomains();
            Assert.That(domainList.Count, Is.EqualTo(5));
        }


        [Test]
        public void using_less_columns_return_null_in_missing_column()
        {
            var adapter = TestDataFacade.GetDomainSimpleLessColumnsAdapter();
            List<DomainSimple> domainList = adapter.GetDomains();
            Assert.That(domainList.Count, Is.EqualTo(5));

            Assert.That(domainList.All(d => d.RTGS == null), Is.True, "All RTGS columns should be null");
        }

        [Test]
        public void required_column_is_missing()
        {
            var adapter = TestDataFacade.GetDomainMissingReqiredColumnsAdapter();
            Assert.Throws<ExcelColumnMissingException>(() => { adapter.GetDomains(); });
        }


        [Test]
        public void more_columns_that_defined()
        {
            var adapter = TestDataFacade.GetDomainSimpleMoreColumnsAdapter();
            var list = adapter.GetDomains();
            Assert.That(list.Count, Is.EqualTo(5));

        }

        [Test]
        public void multiple_sheets()
        {
            var adapter = TestDataFacade.GetCreateBankAdapter();
            var institutions = adapter.GetInstitutions();
            Assert.That(institutions.Count, Is.EqualTo(158));
            Assert.That(institutions[4].RegistrationNumber, Is.EqualTo(8604));

            var settlementBanks = adapter.GetSettlementBanks();
            Assert.That(settlementBanks.Count, Is.EqualTo(158));
            Assert.That(settlementBanks[2].RefShortCode, Is.EqualTo("SC3"));

            var settlementParticipants = adapter.GetSettlementParticpants();
            Assert.That(settlementParticipants[2].SelectParticipant, Is.EqualTo("SC3 bank"));

        }

        [Test]
        public void all_types()
        {
            var adapter = TestDataFacade.GetAllTypeAdapter();
            var allTypes = adapter.GetAllTypes();
            Assert.That(allTypes.Count, Is.EqualTo(5));

            var allType = allTypes[3];
            Assert.That(allType.StringColumn, Is.EqualTo("KKS Bank 1"));
            Assert.That(allType.LongColumn, Is.EqualTo(62874632851));
            Assert.That(allType.IntColumn, Is.EqualTo(23433));
            Assert.That(allType.DateColumn, Is.EqualTo(new DateTime(2015, 03, 13)));
            Assert.That(allType.BoolColumn, Is.False);
            Assert.That(Math.Round(allType.DoubleColumn, 2), Is.EqualTo(Math.Round((double)24.97, 2)));
            
        }

        [Test]
        public void throw_exception_when_sheet_name_doesnt_exist()
        {
            var adapter = TestDataFacade.GetCreateBankAdapter();
            Assert.Throws<ExcelSheetMissingException>(() => adapter.GetSheetMissings());
        }
      
        [Test]
        public void when_column_missing_data_should_read_as_blank()
        {
            ExcelDataReader<MaintainInstitution> list = ExcelDataReader<MaintainInstitution>.Load(Properties.Resources.column_with_only_blanks);
            var newlist = list.AsList();

            var firstRow = newlist[0];
            Assert.That(firstRow.RegistrationNumber, Is.Null);
            Assert.That(firstRow.RegisteredName, Is.EqualTo("VP Securities"));

        }

        [Test]
        public void when_empty_sheet_exist()
        {
            var adapter = TestDataFacade.GetMultipleSheetWhereOneIsEmpty();
            Assert.Throws<ExcelSheetEmptyException>(() => adapter.GetDomains());
        }
    }

}
