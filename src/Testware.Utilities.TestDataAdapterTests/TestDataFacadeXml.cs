﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Testware.Utilities.TestDataAdaptersTests.Adapters;

namespace Testware.Utilities.TestDataAdaptersTests
{
    public class TestDataFacade
    {
        public static AllTypeAdapter GetAllTypeAdapter() 
        { 
            return new AllTypeAdapter(Properties.Resources.domain_simple_with_all_types); 
        }

        public static CreateBankAdapter GetCreateBankAdapter()
        {
            return new CreateBankAdapter(Properties.Resources.create_bank);
        }

        public static DomainAdaptor GetDomainSimpleAdapter()
        {
            return new DomainAdaptor(Properties.Resources.domain_simple);
        }

        public static DomainAdaptor GetDomainSimpleDoubletColumnsAdapter()
        {
            return new DomainAdaptor(Properties.Resources.domain_simple_doublet_columns);
        }

        public static DomainAdaptor GetDomainSimpleLessColumnsAdapter()
        {
            return new DomainAdaptor(Properties.Resources.domain_simple_less_columns);
        }
        public static DomainAdaptor GetDomainMissingReqiredColumnsAdapter()
        {
            return new DomainAdaptor(Properties.Resources.domain_simple_missing_required_column);
        }

        public static DomainAdaptor GetDomainSimpleMoreColumnsAdapter()
        {
            return new DomainAdaptor(Properties.Resources.domain_simple_more_columns);
        }

        public static DomainAdaptor GetDomainSimpleWithMultipleSheetsAdapater()
        {
            return new DomainAdaptor(Properties.Resources.domain_simple_with_multiple_sheets);
        }

        public static DomainXmlAdaptor GetMultipleSheetWhereOneIsEmpty()
        {
            return new DomainXmlAdaptor(Properties.Resources.multiple_sheets_where_one_sheet_is_empty);
        }

    }
}
