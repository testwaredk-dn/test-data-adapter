﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Testware.Utilities.TestDataAdapters;
using Testware.Utilities.TestDataAdaptersTests.Models;

namespace Testware.Utilities.TestDataAdaptersTests.Adapters
{
    public class CreateBankXmlAdapter : BaseExcelAdapter
    {
        public CreateBankXmlAdapter(string resource) : base(resource) { }

        public List<Institution> GetInstitutions()
        {
            ExcelDataReader<Institution> reader = ExcelDataReader<Institution>.Load(Resource);
            return reader.AsList();
        }

        public List<SettlementBank> GetSettlementBanks()
        {
            ExcelDataReader<SettlementBank> reader = ExcelDataReader<SettlementBank>.Load(Resource);
            return reader.AsList();
        }

        public List<SettlementParticipant> GetSettlementParticpants()
        {
            ExcelDataReader<SettlementParticipant> reader = ExcelDataReader<SettlementParticipant>.Load(Resource);
            return reader.AsList();
        }

        public List<MetaDataItem> GetMetaDataItems()
        {
            ExcelDataReader<MetaDataItem> reader = ExcelDataReader<MetaDataItem>.Load(Resource);
            return reader.AsList();
        }

        public List<SheetMissing> GetSheetMissings()
        {
            ExcelDataReader<SheetMissing> reader = ExcelDataReader<SheetMissing>.Load(Resource);
            return reader.AsList();
        }

    }
}
