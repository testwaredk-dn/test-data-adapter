﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Testware.Utilities.TestDataAdapters;
using Testware.Utilities.TestDataAdaptersTests.Models;

namespace Testware.Utilities.TestDataAdaptersTests.Adapters
{

    public class DomainXmlAdaptor : BaseExcelAdapter
    {
        public DomainXmlAdaptor(string resource) : base(resource) { }

        public List<DomainSimple> GetDomains()
        {
            var reader = ExcelDataReader<DomainSimple>.Load(Resource);
            return reader.AsList();
        }

    }
}
