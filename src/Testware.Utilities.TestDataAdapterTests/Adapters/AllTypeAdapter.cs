﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Testware.Utilities.TestDataAdapters;
using Testware.Utilities.TestDataAdaptersTests.Models;

namespace Testware.Utilities.TestDataAdaptersTests.Adapters
{
    public class AllTypeAdapter : BaseExcelAdapter
    {
        public AllTypeAdapter(byte[] resource) : base(resource) { }


        public List<AllTypes> GetAllTypes()
        {
            var reader = ExcelDataReader<AllTypes>.Load(Resource);
            return reader.AsList();
        }
    }
}
