﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Testware.Utilities.TestDataAdaptersTests.Adapters;

namespace Testware.Utilities.TestDataAdaptersTests
{
    public class TestDataFacadeXml
    {
        public static AllTypeXmlAdapter GetAllTypeAdapter() 
        {
            return new AllTypeXmlAdapter(Properties.Resources.domain_simple_with_all_types1); 
        }

        public static CreateBankXmlAdapter GetCreateBankAdapter()
        {
            return new CreateBankXmlAdapter(Properties.Resources.create_bank1);
        }

        public static DomainXmlAdaptor GetDomainSimpleAdapter()
        {
            return new DomainXmlAdaptor(Properties.Resources.domain_simple1);
        }

        public static DomainAdaptor GetDomainSimpleDoubletColumnsAdapter()
        {
            return new DomainAdaptor(Properties.Resources.domain_simple_doublet_columns);
        }

        public static DomainAdaptor GetDomainSimpleLessColumnsAdapter()
        {
            return new DomainAdaptor(Properties.Resources.domain_simple_less_columns);
        }
        public static DomainAdaptor GetDomainMissingReqiredColumnsAdapter()
        {
            return new DomainAdaptor(Properties.Resources.domain_simple_missing_required_column);
        }

        public static DomainAdaptor GetDomainSimpleMoreColumnsAdapter()
        {
            return new DomainAdaptor(Properties.Resources.domain_simple_more_columns);
        }

        public static DomainAdaptor GetDomainSimpleWithMultipleSheetsAdapater()
        {
            return new DomainAdaptor(Properties.Resources.domain_simple_with_multiple_sheets);
        }
        

    }
}
