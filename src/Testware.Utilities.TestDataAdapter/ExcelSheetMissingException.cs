﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Testware.Utilities.TestDataAdapters
{
    public class ExcelSheetMissingException : Exception
    {
        public ExcelSheetMissingException(string message) : base(message) { }
    }
}
