﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Excel;
using System.Data;
using System.Reflection;
using System.Diagnostics;


namespace Testware.Utilities.TestDataAdapters
{

    public class ExcelDataReader<T>
    {
        DataSet _dataSet;

        private ExcelDataReader()
        {

        }

        public static ExcelDataReader<T> Load(object resource)
        {
            if (resource.GetType() == typeof(byte[]))
                return LoadXSLX((byte[])resource);
            else if (resource.GetType() == typeof(string))
                return LoadXML((string)resource);
            else
                throw new Exception("Type not implemented");
        }

        private static ExcelDataReader<T> LoadXSLX(byte[] resource)
        {
            MemoryStream ms = new MemoryStream(resource);
            ExcelOpenXmlReader reader = (ExcelOpenXmlReader)Excel.ExcelReaderFactory.CreateOpenXmlReader(ms);
            reader.IsFirstRowAsColumnNames = true;
            ExcelDataReader<T> retval = new ExcelDataReader<T>();
            retval._dataSet = reader.AsDataSet();
            return retval;
        }

        private static ExcelDataReader<T> LoadXML(string resource)
        {
            ExcelDataReader<T> retval = new ExcelDataReader<T>();
            retval._dataSet = ExcelXmlHelper.ImportExcelXML(resource, true, true);
            return retval;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="ExcelColumnMissingException"></exception>
        /// <returns></returns>
        public List<T> AsList()
        {
            List<T> list = new List<T>();

            T data = Activator.CreateInstance<T>();
            ExcelSheetNameAttribute classAttr = (ExcelSheetNameAttribute)data.GetType().GetCustomAttributes(true).FirstOrDefault(a => a is ExcelSheetNameAttribute);

            string sheetName = "Sheet1";
            if (classAttr != default(ExcelSheetNameAttribute))
            {
                sheetName = classAttr.Name;
            }

            if (!_dataSet.Tables.Contains(sheetName)) throw new ExcelSheetMissingException(string.Format("The sheet {0} did not exist", sheetName));
            DataTable table = _dataSet.Tables[sheetName];

            foreach (DataRow row in table.Rows)
            {
                T domain = Activator.CreateInstance<T>();

                IEnumerable<PropertyInfo> properties = domain.GetType().GetProperties().Where(prop => Attribute.IsDefined(prop, typeof(ExcelColumnAttribute)));
                

                foreach (PropertyInfo propInfo in properties)
                {
                    ExcelColumnAttribute attr = (ExcelColumnAttribute)propInfo.GetCustomAttributes(true).FirstOrDefault(a => a is ExcelColumnAttribute);

                    if (table.Columns.Contains(attr.Caption))
                    {
                        object value = row[attr.Caption];
                        propInfo.SetValue(domain, ChangeType(value, propInfo.PropertyType), null);
                        //Trace.TraceInformation("Column {0} was read from sheet {1}", attr.Caption, table.TableName);
                    }
                    else if (attr.Required)
                    {
                        throw new ExcelColumnMissingException(string.Format("Column {0} is required for sheet {1}", attr.Caption, table.TableName));
                    }
                    else
                    {
                        Trace.TraceWarning("Column {0} did not exist in sheet {1}", attr.Caption, table.TableName);
                    }
                };
                list.Add(domain);
            }
            return list;
        }


        static private object ChangeType(object value, Type type)
        {

            if (value.GetType().Equals(typeof(System.DBNull))) return null;

            if (value == null && type.IsGenericType) return Activator.CreateInstance(type);

            if (value == null) return null;

            if (type == value.GetType()) return value;

            if (type.IsEnum)
            {
                if (value is string)
                    return Enum.Parse(type, value as string);
                else
                    return Enum.ToObject(type, value);

            }

            if (!type.IsInterface && type.IsGenericType)
            {
                Type innerType = type.GetGenericArguments()[0];
                object innerValue = Convert.ChangeType(value, innerType);
                return Activator.CreateInstance(type, new object[] { innerValue });
            }

            if (value is string && type == typeof(Guid)) return new Guid(value as string);

            if (value is string && type == typeof(Version)) return new Version(value as string);

            if (!(value is IConvertible)) return value;

            return Convert.ChangeType(value, type);

        }
    
    }
}
