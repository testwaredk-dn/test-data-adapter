﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Testware.Utilities.TestDataAdapters
{
    public class ExcelSheetEmptyException : Exception
    {
        public ExcelSheetEmptyException(string message) : base(message) { }
    }
}
