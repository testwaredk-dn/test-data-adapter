﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Testware.Utilities.TestDataAdapters
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ExcelColumnAttribute : Attribute
    {
        private readonly string caption;
        private readonly bool required = false;

        public ExcelColumnAttribute()
        {
            this.caption = "";
        }

        public ExcelColumnAttribute(string Caption)
        {
            this.caption = Caption;
        }

        public ExcelColumnAttribute(string Caption, bool Required)
        {
            this.caption = Caption;
            this.required = Required;
        }

        public string Caption
        {
            get { return caption; }
        }

        public bool Required
        {
            get { return required; }
        }

    }
}
