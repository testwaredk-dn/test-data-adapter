﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Testware.Utilities.TestDataAdapters
{
    public class BaseExcelAdapter
    {
        public object Resource { get; private set; }
        public BaseExcelAdapter(object resource) { Resource = resource; }

    }
}
