﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Testware.Utilities.TestDataAdapters
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ExcelSheetNameAttribute : Attribute
    {
        private readonly string name;

        public ExcelSheetNameAttribute()
        {
            this.name = "Sheet1";
        }

        public ExcelSheetNameAttribute(string Name)
        {
            this.name = Name;
        }

        public string Name
        {
            get { return name; }
        }

    }
}
