﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Testware.Utilities.TestDataAdapters
{
    public class ExcelColumnMissingException : Exception
    {
        public ExcelColumnMissingException(string message) : base(message) { }
    }
}
