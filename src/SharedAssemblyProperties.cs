﻿using System.Reflection;

[assembly: AssemblyCompany("Testware ApS")]
[assembly: AssemblyCopyright("Copyright ©  2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyProduct("Testware.Utilties.TestDataAdapter")]

// Version information for an assembly consists of the following four values:
[assembly: AssemblyVersion("1.1.6")]