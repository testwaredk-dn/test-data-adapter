@echo off
setlocal
if exist src\packages rmdir src\packages /s /q

set nuget=".nuget\NuGet.exe"
cd src
%nuget% restore
